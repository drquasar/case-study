# Magento 2 Product Attribute Module

    ``quasar/module-product-attribute``

## Main Functionalities
Adds new product attribute for simple and configurable types. Created attribute can be changed via API endpoint.

## Installation
 - `composer require quasar/module-product-attribute`
 - `php bin/magento setup:upgrade`
 - `php bin/magento setup:di:compile`
 - `php bin/magento c:c`
 - `php bin/magento c:f`

## API usage
First get admin user token:
- URL: {website_url}/rest/V1/integration/admin/token
- Method: POST
- Header:
``
{
  "Content-Type":"application/json"
}
``
- Body:
``
{
    "username":<admin_username>,
    "password":<admin_password>
}
``
- Response:
``
<admin_token>
``

Custom attribute changing

- URL : {website_url}/rest/V1/quasar_productattribute/setCustomAttribute
- Method : POST
- Header :
``
{
  "Content-Type":"application/json",
  "Authorization":"Bearer <admin_token>"
}
``
- Body : 
``
{
    "productId":<product_id>,
    "customAttributeValue":<new_value>
}
``
- response: 
``
[
    true,
    "The custom attribute has been updated successfully."
]
``
