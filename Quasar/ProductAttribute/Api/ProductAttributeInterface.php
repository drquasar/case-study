<?php
namespace Quasar\ProductAttribute\Api;

interface ProductAttributeInterface {

    /**
     * POST for setting custom attribute for the products.
     *
     * @param string $productId
     * @param string $customAttributeValue
     * @return string[]
     */
    public function setCustomAttribute($productId, $customAttributeValue);

}