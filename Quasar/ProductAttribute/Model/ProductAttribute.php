<?php
namespace Quasar\ProductAttribute\Model;

use Quasar\ProductAttribute\Api\ProductAttributeInterface;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Quasar\ProductAttribute\Logger\Logger;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ProductAttribute
 */
class ProductAttribute implements ProductAttributeInterface {

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param Action $productAction
     * @param StoreManagerInterface $storeManager
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Logger $logger,
        Action $productAction,
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->logger = $logger;
        $this->productAction = $productAction;
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
    }

    public function setCustomAttribute($productId, $customAttributeValue) {
        $loggerEnabled = $this->scopeConfig->getValue('productattribute/general/enablelog', ScopeInterface::SCOPE_STORE);
        $response = ['success' => false];
        try {
            $productType = $this->productRepository->getById($productId)->getTypeId();
            if ($productType == 'simple') {
                $this->productAction->updateAttributes(
                    [$productId],
                    ['simple_custom_attribute' => $customAttributeValue],
                    $this->storeManager->getStore()->getId()
                );
                $response = ['success' => true, 'message' => 'The custom attribute has been updated successfully.'];
                if ($loggerEnabled) $this->logger->info('The custom attribute has been updated successfully. Product id: '.$productId.', type: '.$productType.', new value: '.$customAttributeValue);
            } else if ($productType == 'configurable') {
                $this->productAction->updateAttributes(
                    [$productId],
                    ['configurable_custom_attribute' => $customAttributeValue],
                    $this->storeManager->getStore()->getId()
                );
                $response = ['success' => true, 'message' => 'The custom attribute has been updated successfully.'];
                if ($loggerEnabled) $this->logger->info('The custom attribute has been updated successfully. Product id: '.$productId.', type: '.$productType.', new value: '.$customAttributeValue);
            } else {
                $response = ['success' => false, 'message' => 'The product is not simple or configurable.'];
                if ($loggerEnabled) $this->logger->info('The product is not simple or configurable. Product id: '.$productId.', type: '.$productType);
            }
        } catch (NoSuchEntityException $e) {
            $response = ['success' => false, 'message' => 'There is no such product.'];
            if ($loggerEnabled) $this->logger->info('There is no such product. Product id: '.$productId);
        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            if ($loggerEnabled) {
                $this->logger->info('Product id: '.$productId);
                $this->logger->info($e->getMessage());
            }
        }
        return $response;
    }

}